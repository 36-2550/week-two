#include <iostream>

using namespace std;

void draw_whole(int height){
  for(int i = 0; i < height - 1; i++){
    cout << "*";
  }
  cout << endl;
}

void draw_empty(int height){
  for(int i = 0; i < height; i++){
    if(i == 0 || i == height -1){
      cout << "*";
    }
    else{
      cout << " ";
    }
  }
  cout << endl;
}
//render
void render_square(int height){
  for(int i = 0; i < height; i++){
    if(i == 0 || i == height -1){
      draw_whole(height);
    }
    else{
      draw_empty(height);
    }
  }
}

void fizzbuzz(){
  for(int i = 0; i < 100; i++){
    if(i % 15 == 0){
      cout << "FizzBuzz" << endl;
    }
    if(i % 5 == 0){
      cout << "Buzz" << endl;
    }
    if(i % 3 == 0){
      cout << "Fizz" << endl;
    }
    else{
      cout << i << endl;
    }
  }
}
int main(){
  render_square(8);
  return 0;
}

